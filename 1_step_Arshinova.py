import numpy as np
from astropy.io import fits
import glob

bias_list = glob.glob('Bias*fit')
flat_list = glob.glob('FF*fit')
nine_list = glob.glob('9*fit')
zero_list = glob.glob('0*fit')
G_list = glob.glob('G*fit')
J_list = glob.glob('J*fit')
R_list = glob.glob('R*fit')
S_list = glob.glob('S*fit')
T_list = glob.glob('T*fit')
sci_exp_list = nine_list + zero_list + G_list + J_list + R_list + S_list + T_list
all_images_list = bias_list + flat_list + sci_exp_list
raw_image_data = dict()
for image_name in all_images_list: raw_image_data[image_name] = fits.getdata(image_name)
biascube = np.stack([raw_image_data[bias_frame] for bias_frame in bias_list], axis=0)
master_bias = np.median(biascube, axis=0)
debias_list_in = sci_exp_list + flat_list
debias_list_out = ['debiased_' + im for im in debias_list_in]
debias_data_out = dict()
for i in range(len(debias_list_in)):
    debias_data_out[debias_list_out[i]] = raw_image_data[debias_list_in[i]] - master_bias
debiascube = np.stack([debias_data_out[image] for image in debias_list_out], axis=0)
debias_sci_exp_list = ['debiased_' + im for im in sci_exp_list]
flatcube = np.stack([raw_image_data[bias_frame] for bias_frame in bias_list], axis=0)
master_flat = np.median(flatcube, axis=0)
norm_master_flat = (master_flat - np.min(master_flat)) / (np.max(master_flat) - np.min(master_flat))
norm_master_flat = np.where(norm_master_flat == 0, np.median(norm_master_flat), norm_master_flat)
f_debias_sci_exp_list = ['f_' + im for im in debias_sci_exp_list]
f_debias_data_out = dict()
for i in range(len(debias_sci_exp_list)):
    f_debias_data_out[f_debias_sci_exp_list[i]] = debias_data_out[debias_sci_exp_list[i]] / norm_master_flat
